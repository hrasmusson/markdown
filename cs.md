# *SHET*

*nej*

# **det finns en strumpa i ditt rum**


# ***värför inte***

# `det är inte trevligt`

`/g ye`

Det här dokumentet är skrivet i ett format som kallas *Markdown*. Markdown är ett enkelt sätt att formattera textdokument.

## Rubriker

För att göra en rubrik skriver man en hash `#` i början av raden, som i början av rubriken i detta avsnitt.

Ju fler `#` i början av raden, desto mindre rubrik.

Skriv en `#` i början av raden där det står *Rubrik 1*, `##` på raden
*Rubrik 3* och `###` på raden *Rubrik 3*.

# It's time to play the music

## It's time to light the lights

### It's time to meet the Muppets on the Muppet Show tonight!

## Betoning

För att få viss ord att sticka ut kan man betona det genom att skriva en
asterisk `*` framför och efter ordet.

Betona ordet emphasis i denna mening genom att omge det med `*`.

Betona orden strong emphasis i denna mening genom att omge dem båda med `**`.

![slinky](http://upload.wikimedia.org/wikipedia/commons/d/d3/Newtons_cradle_animation_book_2.gif)

## Listor

Det finns två typer av listor:

* Punktlistor
* Numrerade listor

Punktlistor börjar med en asterisk på varje rad. Man kan göra en fruktsallad av
ingredienserna nedan. Gör om dem till en punktlista.

* Chiquita Banan
* Vindruvor
* Kiwi
* Passionsfrukt
* Mango

Numrerade listor börjar med nummer och punkt. Listan nedan är inte komplett.
Lägg till punkterna 3 - 5 så att det blir rätt.

1. Skala kycklingen och skär den i skivor.
2. Skölj såsen och dela den.
3. Skala habaneron och skär i tärningar.
4. Gröp ur din hjärna.
5. Skala mangon och skär den som ett lasersvärd.
6. Blanda alla ingridienser i en skål.
7. ÄT.

så här ska det se ut:

![habanero](http://files.coloribus.com/files/adsarchive/part_300/3003805/file/tabasco-habanero-sauce-ship-small-46542.jpg)


## Kod

Genom att börja en rad med fyra blanksteg eller en tab gör man att texten blir
ett kodblock.

Skriv 4 blanksteg i början av nedanstående rader för att göra ett kodblock.

    x = 3
    y = 5
    if x > y:
    print("x is bigger than y")
	else:
    print("y is bigger than x")

Kodavsnitt kan även göras genom att ange text inom ` tecken.

`titta en hund!`
![hund](http://vikdjur4.files.wordpress.com/2012/11/igelkott.jpg)

Denna rad innehåller lite `kod som renderas annorlunda`. Gör ett kodavsnitt av
resten av raden: x = foo() + bar

## Länkar
[ye](http://my.patwic.com/hrasmusson/ye.md)
Du kan göra en länk till en annan sida genom att ange texten som ska vara en
länk inom hakparanteser, [ och ]. Efter texten inom hakparanteser ska det finnas
en address till sidan du ska länka till inom vanliga paranteser ( och ).

`En länk till [Markdown Syntax](http://daringfireball.net/projects/markdown/syntax)`

blir

En länk till [Markdown Syntax](http://daringfireball.net/projects/markdown/syntax)

Gör ordet [Psyklopedin](http://psyklopedin.org/wiki/Huvudsida) i denna mening till en fågel.

## Bilder

En länk till en bild gör du så här:

![elofant](http://dl.dropbox.com/u/52918745/Pets/Exotic/Elephant.png)

![elofant](http://dl.dropbox.com/u/52918745/Pets/Exotic/Elephant.png)

![elofant](http://dl.dropbox.com/u/52918745/Pets/Exotic/Elephant.png)


Texten inom hakparanteserna är en bildtext och bildens address anges inom
paranteser.

Gör en länk till en valfri bild på en elefant istället för texten på denna rad.

[info om hundar klicka här](http://psyklopedin.org/wiki/Hund)
